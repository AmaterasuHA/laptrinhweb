﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LapTrinhWeb.Controllers
{
    public class QuanLyBaoCaoController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MonthlyIntendedOrder()
        {
            ViewBag.Message = "something";
            return View();
        }

        public ActionResult BaoCaoDuTruThietBi()
        {
            ViewBag.Message = "bao cao du tru thiet bi";
            return View();
        }

        public ActionResult BaoCaoNhapMoiThietBi()
        {
            ViewBag.Message = "bao cao nhap moi thiet bi";
            return View();
        }

        public ActionResult BaoCaoDieuChuyenThietBi()
        {
            ViewBag.Message = "bao cao dieu chuyen thiet bi";
            return View();
        }
    }
}